# DIP assigment:

Image Processing - SCC0251

Assigment 2 - Image Enhancement and Filtering

Author:
* Lucas Nobuyuki Takahashi

Folders and files:
* [Python code](./submission/dip02-submission.py) contains the Python code for run.codes submission
* [Images](/images) contains images used in this demos
* [Notebook with Demo](dip02.ipynb) is a notebook exemplifying functions developed and submitted
